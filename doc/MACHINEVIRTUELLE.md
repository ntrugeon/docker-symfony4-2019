## Prérequis
- Vous avez déjà tous les logiciels installés.

## Spécificités

1. Vous stockerez vos fichiers dans /home/$USER
1. Il faut refaire certaines manipulations à chaque début de TP :
    - git config...
    - ajout des clés ssh
    - construction des images docker
    - récupération de vos fichiers symfony
    - récupération de votre base de données

## Installation

1. Dupliquer le projet :
    ```bash
    git clone https://gitlab.univ-lr.fr/ntrugeon/docker-symfony4-2019.git
    # renommer le dossier
    mv docker-symfony4-2019 mon_projet (tp1Symfony pour la première fois)
    # on se place dans la bon dossier
    cd mon_projet
    ```

2. Modifier le ficher .env pour activer le proxy et mettre l'UID à 1001
   - PROXY=http://wwwcache.univ-lr.fr:3128 (à décommenter)
   - UID=1001 (à modifier 1000 par défaut)

3. Construire et exécuter les conteneurs (Cela peut prendre un peu de temps)

    ```bash
    $ sudo docker-compose build
    $ sudo docker-compose up -d
    ```

4. Installer Symfony
    1. On installe symfony en version minimale avec Composer. On installe les composants nécessaires à nos applications également avec Composer

        ```bash
        $ sudo docker-compose exec php bash
        $ composer create-project symfony/skeleton symfony
        $ cd symfony
        $ composer require --dev profiler maker
        $ composer require annotations twig orm form validator
        $ composer require symfony/apache-pack
        ```
    
    2. Ouvrir le dossier symfony du projet dans phpStorm, et créer **.env.local** en ajoutant la ligne DATABASE_URL comme ceci :

        ```yml
        DATABASE_URL=mysql://symfony:symfony@db:3306/symfony
        ```

    3. Modifiez également le fichier **config/packages/doctrine.yaml** pour dire qu'on utilise mariadb à la place de mysql
        ```yml
        server_version: 'mariadb-10.1.36'
        ```

    4. En profiter pour installer dans Phpstorm les plugins symfony et .env pour profiter pleinement de votre IDE.

5. C'est parti :-)

## Je commence à travailler sur le projet

Vous avez juste à exécuter `sudo docker-compose up -d`, puis:

* Application symfony [localhost:8000](http://localhost:8000)
* Logs du serveur web : logs/nginx

## Je finis de travailler sur le projet

Vous avez juste à exécuter `sudo docker-compose down`.

Pensez à envoyer sur votre git les données.

** OU **

Comme vous avez connecté votre lecteur personnel à la machine virtuelle, vous pouvez sauvegarder votre dossier du TP hormis le dossier **vendor** et **var** dans votre répertoire personnel.
Ce dossier contient tous les fichiers symfony dans le dossier symfony et toute votre base de données dans le dossier data.

## Reprise du travail sur une autre séance

Suivez la [doc associée](REPRISETRAVAIL.md)

## Comment cela fonctionne ?

Vous pouvez aller regarder le fichier `docker-compose.yml`, avec les images `docker-compose` correspondantes:

* `db`: le container mariadb 10.1,
* `php`: php-fpm en version 7.2,
* `apache`: le serveur web apache2 sur le port 8000,
* `nginx`: le serveur web nginx sur le port 8001 (en commentaire par défaut)

## Commandes utiles

```bash
# On rentre dans un conteneur en bash
$ sudo docker-compose exec php bash

# Mise à jour de Composer
$ sudo docker-compose exec php composer update

# Commandes symfony
$ sudo docker-compose exec php bash
$ cd symfony
$ sf make:controller
$ sf make:entity
...

# Supprimer tous les conteneurs (en cas de gros plantage, à utiliser en dernier recours)
$ docker rm $(docker ps -aq)

# Supprimer toutes les images (en cas de gros plantage, à utiliser en dernier recours)
$ docker rmi $(docker images -q)
```

## FAQ
* Je ne comprends rien, que faire ?
Allez voir votre prof !

* Xdebug?
Xdebug est déjà configuré
Il faut ajouter le module Xdebug helper pour Firefox ou pour Chrome
Il faut également configurer Phpstorm en se connectant au port  `9001` avec l'id `PHPSTORM`. Vous pouvez suivre ce [lien](https://blog.eleven-labs.com/fr/debug-run-phpunit-tests-using-docker-remote-interpreters-with-phpstorm/). Le dépôt que vous utilisez est déjà paramétré. Utilisez docker-compose à la place de docker dans le "Remote" de l'interpréteur PHP.
