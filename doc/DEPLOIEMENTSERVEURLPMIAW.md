# Mise en production d'un site Symfony sur le serveur Lpmiaw

## Attention

Une fois le site en place, vous ne devez pas modifié vos fichiers sources directement sur le serveur. On considère que ce serveur est un serveur de production.

Il ne doit utiliser que la branche master git de votre projet et se synchroniser avec elle.

## Prérequis

1. Avoir généré une clé ssh pour son compte sur le serveur : ssh-keygen
1. Avoir ajouté cette clé dans son profil gitlab.
1. Avoir une branche master propre et fonctionnelle sur sa machine

## Récupération des fichiers

```sh
git clone url_mon_projet_symfony
```

## Installation des dépendances

```sh
cd mon_projet_symfony
composer install
```

## Paramétrages

- Créez un fichier .env.local en spécifiant l'accès à la base de données et éventuellement les autres paramètres de votre application.
- Lancer les migrations
- Lancez les fixtures pour peupler votre base de données.

## Enjoy !