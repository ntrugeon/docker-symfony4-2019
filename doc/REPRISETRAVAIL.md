# Reprise d'un travail déposé sur Git sur une nouvelle machine

1. chargez votre clé privée ssh depuis un support amovible

```sh
$ sudo cat /chemin_vers_votre_dossier_ssh/id_rsa | ssh-add -

```
**OU**

générez une nouvelle clé en l'ajoutant aux paramètres de votre compte gitlab.

```sh
$ ssh-keygen

```

2. Faites un git clone de votre projet hébergé sur le gitlab du l'Université

**OU**

Dans phpstorm, vous pouvez choisir VCS->Checkout From Version Control->Git en collant votre dépôt git.

**OU**

Vous avez sauvegardé votre dossier de travail sur une clé, disque, espace perso... Vous pouvez copier/coller votre dossier symfony dans le dossier de votre projet.

3. Construisez votre docker

``` sh
$ docker-compose build
$ docker-compose up -d
$ docker-compose exec php bash
# dans le conteneur php
$ cd symfony
$ composer install
```

4. Fichier .env.local

Vérifiez ou créez un fichier .env.local en mettant l'accès à la base et les configurations propres à votre application.

5. Votre base de données

Elle se trouve dans le dossier data. Si vous l'avez sauvegardé ou commité, vous retrouverez vos données.

6. Votre application doit fonctionner.

7. Continuez votre travail avec phpstorm.
