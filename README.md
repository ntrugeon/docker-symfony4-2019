# Docker Symfony (PHP7-FPM - NGINX - MariaDB)

Cet environnement correspond à l'environnement de production du serveur lpmiaw.univ-lr.fr à savoir :

    - php 7.2
    - mariadb 10.1
    - apache 2.4
    - de manière facultative nginx

Docker-symfony4 vous donne tout ce que vous avez besoin pour développer des applications sous Symfony 4.
C'est une architecture complète à utiliser avec docker et [docker-compose](https://docs.docker.com/compose/).

## Type de machines

1. [Machine virtuelle linux de l'Université](doc/MACHINEVIRTUELLE.md)
1. [Machine linux personnelle](doc/MACHINEPERSOLINUX.md)
1. [Machine mac personnelle](doc/MACHINEPERSOMAC.md)
1. [Machine windows personnelle](doc/MACHINEPERSOWINDOWS.md)

## Mise en production

1. [Mise en production sur le serveur LPMiaw](doc/DEPLOIEMENTSERVEURLPMIAW.md)